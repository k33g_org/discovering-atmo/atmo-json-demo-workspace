use suborbital::runnable::*;
use serde_json::json;

struct Restaurants{}

impl Runnable for Restaurants {
    fn run(&self, _: Vec<u8>) -> Result<Vec<u8>, RunErr> {

        let restaurants = json!([
            {"name": "Le Ciel", "address": "17 rue Alexandre Dumas, 75011 Paris", "phone": "(33)664 441 416"},
            {"name": "A La Renaissance", "address": "87 rue de la Roquette, 75011 Paris", "phone": "(33)143 798 309"},
            {"name": "La Cave de l'Insolite", "address": "30 rue de la Folie Méricourt, 75011 Paris", "phone": "(33)153 360 833"}
        ]);

        suborbital::resp::content_type("application/json; charset=utf-8");
    
        Ok(restaurants.to_string().as_bytes().to_vec())
    }
}


// initialize the runner, do not edit below //
static RUNNABLE: &Restaurants = &Restaurants{};

#[no_mangle]
pub extern fn _start() {
    use_runnable(RUNNABLE);
}
