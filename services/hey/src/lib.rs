use suborbital::runnable::*;
use serde_json::json;

struct Hey{}

impl Runnable for Hey {
    fn run(&self, _: Vec<u8>) -> Result<Vec<u8>, RunErr> {
    
        let message = json!({
            "text": "👋 Hello World 🌏",
            "author": "@k33g"
        });

        suborbital::resp::content_type("application/json; charset=utf-8");

        Ok(message.to_string().as_bytes().to_vec())

    }
}


// initialize the runner, do not edit below //
static RUNNABLE: &Hey = &Hey{};

#[no_mangle]
pub extern fn _start() {
    use_runnable(RUNNABLE);
}
