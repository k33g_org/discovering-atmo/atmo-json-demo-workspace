# last update: November 20 2021
FROM gitpod/workspace-full

USER gitpod

RUN brew tap suborbital/subo && \
    brew install subo && \
    brew install exa && \
    brew install bat && \
    brew install httpie && \
    brew install hey

