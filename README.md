# Atmo Workspace Json Payload

## What is Atmo?

**[Atmo](https://github.com/suborbital/atmo)** is a project from [Suborbital](https://suborbital.dev/). 

To say it short, Atmo is a toolchain allowing writing and deploying wasm microservices smoothly.
With Atmo, you can write microservices in Rust, Swift, and AssemblyScript without worrying about complicated things, and you only have to care about your source code:

- No fight with wasm and string data type
- No complex toolchain to install
- Easy deployment with Docker (and then it becomes trivial to deploy wasm microservices on Kubernetes)
- ...

## About this project

This is a running demo to demonstrate how to serve a JSON REST API with Atmo.

This project contains 2 Runnables (or functions) written in Rust:

- `hello` (see `./services/hello/src/lib.rs`) that produces a JSON object from a structure and the **serde** library
- `hey` (see `./services/hey/src/lib.rs`) that produces a JSON object with the `json!` macro and the **serde** library
- `restaurants` (see `./services/restaurants/src/lib.rs`) that produces an Array of JSON objects

> 👋 you need to put the required dependencies in the `Cargo.toml` file:
> ```
> serde = { version = "1.0", features = ["derive"] }
> serde_json = "1.0"
> ```


### 🤚 To begin, click on the **Gitpod** button: [![Open in GitPod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/k33g_org/discovering-atmo/atmo-json-demo-workspace)

## How to build the Runnables

```bash
cd services
subo build .
```

## Serve the Runnables

```bash
cd services
subo dev
```

## Call the Service

### Get a json object

```bash
http http://localhost:8080/hello
```

> Expected result:
```bash
Content-Length: 49
Content-Type: application/json; charset=utf-8
Date: Tue, 26 Oct 2021 13:26:24 GMT

{
    "author": "@k33g",
    "text": "👋 Hello World 🌏"
}
```

🖐 Try this one too: 
```bash
http http://localhost:8080/hey
```

### Get a json array

```bash
http http://localhost:8080/restaurants
```

> Expected result:
```bash
Content-Length: 309
Content-Type: application/json; charset=utf-8
Date: Tue, 26 Oct 2021 14:37:31 GMT

[
    {
        "address": "17 rue Alexandre Dumas, 75011 Paris",
        "name": "Le Ciel",
        "phone": "(33)664 441 416"
    },
    {
        "address": "87 rue de la Roquette, 75011 Paris",
        "name": "A La Renaissance",
        "phone": "(33)143 798 309"
    },
    {
        "address": "30 rue de la Folie Méricourt, 75011 Paris",
        "name": "La Cave de l'Insolite",
        "phone": "(33)153 360 833"
    }
]
```
